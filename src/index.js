import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'

import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'

import App from './components/App'

// import reducer from './reducers'
import reducer, { watchRequestUsers } from './ducks'

const sagaMiddleware = createSagaMiddleware()
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(
	reducer,
	composeEnhancers(applyMiddleware(sagaMiddleware))
)

// then run the saga
sagaMiddleware.run(watchRequestUsers)

const root: ?Element = document.getElementById('root')

if (root)
	ReactDOM.render(
		<Provider store={store}>
			<App />
		</Provider>,
		root
	)
