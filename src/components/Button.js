import React from 'react'
import styled from 'styled-components'

const StyledButton = styled.button`
	cursor: pointer;
	color: #fff;
	background-color: #2196f3;
	box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.2),
		0px 2px 2px 0px rgba(0, 0, 0, 0.14),
		0px 3px 1px -2px rgba(0, 0, 0, 0.12);
	padding: 8px 16px;
	font-size: 0.875rem;
	box-sizing: border-box;
	transition: background-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,
		box-shadow 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,
		border 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
	font-weight: 500;
	font-family: 'Roboto', 'Helvetica', 'Arial', sans-serif;
	line-height: 1.4em;
	border: 0px;
	border-radius: 4px;
	text-transform: uppercase;

	:hover {
		background-color: #1976d2;
	}
`

const Button = ({
	loading,
	onClick
}: {
	loading: boolean,
	onClick: Function
}) => {
	return (
		<StyledButton onClick={onClick} disabled={loading}>
			Generate
		</StyledButton>
	)
}

export default Button
