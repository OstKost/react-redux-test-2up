import React from 'react'
import styled from 'styled-components'
import { connect } from 'react-redux'
import { fetchUsers } from '../ducks'
import UserItem from './UserItem'
import Button from './Button'

const List = styled.ul`
	list-style: none;
	display: flex;
	flex-direction: column;
	align-items: center;
	padding: 0 20px;
`

const renderUsers = (
	users: Array<{ name: string, id: number }>
): Array<typeof UserItem> =>
	users.map(({ name, id }): any => <UserItem key={id} name={name} />)

const UserList = ({
	users,
	loading,
	error,
	dispatch
}: {
	users: Array<{ name: string, id: number }>,
	loading: boolean,
	error: string,
	dispatch: Function
}) => (
	<List>
		{users.length ? (
			renderUsers(users)
		) : !loading ? (
			<h3>Click button to generate a fake user.</h3>
		) : null}

		{loading ? <p>Loading...</p> : null}
		{error ? <h3>{error}</h3> : null}

		<Button
			onClick={() => dispatch(fetchUsers(users.length + 1))}
			loading={loading}
		/>
	</List>
)

export default connect(state => state)(UserList)
