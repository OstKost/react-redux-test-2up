import React from 'react'
import styled from 'styled-components'
import Header from './Header'
import UserList from './UserList'

const AppWrapper = styled.div`
	text-align: center;
`

const App = () => (
	<AppWrapper>
		<Header />
		<UserList />
	</AppWrapper>
)

export default App
