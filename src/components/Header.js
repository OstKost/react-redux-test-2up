import React from 'react'
import styled, { keyframes } from 'styled-components'
import logo from '../images/logo.svg'

const HeaderWrapper = styled.header`
	background-color: #282c34;
	min-height: 30vh;
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	font-size: calc(10px + 2vmin);
	color: white;
`

const rotateLogo = keyframes`
	from {
		transform: rotate(0deg);
	}
	to {
		transform: rotate(360deg);
	}
`

const Logo = styled.img`
	animation: ${rotateLogo} infinite 20s linear;
	height: 10vmin;
`

const Header = () => {
	return (
		<HeaderWrapper>
			<Logo src={logo} alt="logo" />
			<h3>React Contact Manager</h3>
		</HeaderWrapper>
	)
}

export default Header
