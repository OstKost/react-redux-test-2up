import React from 'react'
import styled from 'styled-components'

const ListItem = styled.li`
	width: 100%;
	max-width: 450px;
	padding: 7px;
	margin-bottom: 10px;
	background-color: lightgray;
	border: 1px solid grey;
	text-align: left;
`

const UserItem = ({ name }: { name: string }) => <ListItem>{name}</ListItem>

export default UserItem
