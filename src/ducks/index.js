import { call, put, takeEvery } from 'redux-saga/effects'
import jsonPlaceholder from '../apis/jsonPlaceholder'

// Actions
const FETCH_USERS = 'fake-users/users/FETCH_USERS'
const REQ_USERS = 'fake-users/users/REQ_USERS'
const REQ_USERS_SUCCEEDED = 'fake-users/users/REQ_USERS_SUCCEEDED'
const REQ_USERS_FAILED = 'fake-users/users/REQ_USERS_FAILED'

// Reducer
const INITIAL_STATE = {
	users: [],
	loading: false,
	error: ''
}

export default function reducer(
	state: {
		users: Array<{ name: string }>,
		loading: boolean,
		error: string
	} = INITIAL_STATE,
	action: { type: string, user: { name: string }, error: string }
) {
	switch (action.type) {
		case REQ_USERS:
			return { ...state, loading: true, error: '' }
		case REQ_USERS_SUCCEEDED:
			return {
				users: [...state.users, action.user],
				loading: false,
				error: ''
			}
		case REQ_USERS_FAILED:
			return { ...state, loading: false, error: action.error }
		default:
			return state
	}
}

// Action Creators
export const requestUsers = () => {
	return { type: REQ_USERS }
}

export const requestUsersSuccess = (user: { name: string }) => {
	return { type: REQ_USERS_SUCCEEDED, user }
}

export const requestUsersError = (error: string) => {
	return { type: REQ_USERS_FAILED, error }
}

export const fetchUsers = (userId: number) => {
	return { type: FETCH_USERS, userId }
}

// side effects
function* fetchUsersSaga(action): any {
	try {
		yield put(requestUsers())
		const data = yield call(() => {
			return jsonPlaceholder
				.get(`/users/${action.userId}`)
				.then(response => response.data)
		})
		yield put(requestUsersSuccess(data))
	} catch (error) {
		yield put(requestUsersError(error.message))
	}
}

export function* watchRequestUsers(): any {
	yield takeEvery(FETCH_USERS, fetchUsersSaga)
}
